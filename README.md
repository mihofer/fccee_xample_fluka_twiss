# fccee_xample_fluka_twiss

Small example how to obtain a twiss file for an arccell required as input for FLUKA studies.

## Getting started

To setup, install the required python packages using

`python -m pip install -r python_requirements.txt`.

Then to run and play with the example, use

`jupyter notebook`

to open a jupyter instance.

Alternatively, the script can be run on SWAN, provided a CERN account.

<a href="https://cern.ch/swanserver/cgi-bin/go/?projurl=https://gitlab.cern.ch/mihofer/fccee_xample_fluka_twiss.git" target="_blank">
  <img alt="" src="https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png">
</a>
